<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTestSessionResult extends Model
{
    //
    public function testSession()
    {
        return $this->belongsTo(UserTestSession::class);
    }
}
