<?php

namespace App\Http\Controllers;

use App\UserTestSessionResult;
use Illuminate\Http\Request;

class UserTestSessionResultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserTestSessionResult  $userTestSessionResult
     * @return \Illuminate\Http\Response
     */
    public function show(UserTestSessionResult $userTestSessionResult)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserTestSessionResult  $userTestSessionResult
     * @return \Illuminate\Http\Response
     */
    public function edit(UserTestSessionResult $userTestSessionResult)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserTestSessionResult  $userTestSessionResult
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserTestSessionResult $userTestSessionResult)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserTestSessionResult  $userTestSessionResult
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserTestSessionResult $userTestSessionResult)
    {
        //
    }
}
