<?php

namespace App\Http\Controllers;

use App\Test;
use Illuminate\Http\Request;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Test::with('user')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Mock user id;
        $request_data = $request->all();
        $request_data['created_by'] = 1; // Get from authenticated user
        $request_data['total_points'] = 0; // Must be generated according to the number of questions and options

        // Do your validation here
        // https://laravel.com/docs/5.8/validation#validating-arrays
        // var_dump or debug is your friend
        // dd($request_data);
        $test_object = Test::create($request_data);
        $questions = $request_data['questions']; // Get the questions array
        if ($questions) {
            foreach ($questions as $question) {
                $options = $question['options'];
                $question = $test_object->testQuestions()->create($question);
                foreach ($options as $option) {
                    $option = $question->testQuestionOptions()->create($option);
                }
            }
        }

        return [
            'success' => true,
            'message' => 'Successfully saved test'
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function show(Test $test)
    {
        return $test->load('user', 'testQuestions', 'testQuestions.testQuestionOptions');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function edit(Test $test)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Test $test)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function destroy(Test $test)
    {
        //
    }
}
