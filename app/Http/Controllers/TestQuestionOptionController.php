<?php

namespace App\Http\Controllers;

use App\TestQuestionOption;
use Illuminate\Http\Request;

class TestQuestionOptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TestQuestionOption  $testQuestionOption
     * @return \Illuminate\Http\Response
     */
    public function show(TestQuestionOption $testQuestionOption)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TestQuestionOption  $testQuestionOption
     * @return \Illuminate\Http\Response
     */
    public function edit(TestQuestionOption $testQuestionOption)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TestQuestionOption  $testQuestionOption
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TestQuestionOption $testQuestionOption)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TestQuestionOption  $testQuestionOption
     * @return \Illuminate\Http\Response
     */
    public function destroy(TestQuestionOption $testQuestionOption)
    {
        //
    }
}
