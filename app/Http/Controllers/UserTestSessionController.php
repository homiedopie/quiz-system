<?php

namespace App\Http\Controllers;

use App\UserTestSession;
use Illuminate\Http\Request;

class UserTestSessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserTestSession  $userTestSession
     * @return \Illuminate\Http\Response
     */
    public function show(UserTestSession $userTestSession)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserTestSession  $userTestSession
     * @return \Illuminate\Http\Response
     */
    public function edit(UserTestSession $userTestSession)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserTestSession  $userTestSession
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserTestSession $userTestSession)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserTestSession  $userTestSession
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserTestSession $userTestSession)
    {
        //
    }
}
