<?php

namespace App\Http\Controllers;

use App\UserTestSessionAnswer;
use Illuminate\Http\Request;

class UserTestSessionAnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserTestSessionAnswer  $userTestSessionAnswer
     * @return \Illuminate\Http\Response
     */
    public function show(UserTestSessionAnswer $userTestSessionAnswer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserTestSessionAnswer  $userTestSessionAnswer
     * @return \Illuminate\Http\Response
     */
    public function edit(UserTestSessionAnswer $userTestSessionAnswer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserTestSessionAnswer  $userTestSessionAnswer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserTestSessionAnswer $userTestSessionAnswer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserTestSessionAnswer  $userTestSessionAnswer
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserTestSessionAnswer $userTestSessionAnswer)
    {
        //
    }
}
