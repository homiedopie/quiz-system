<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTestSessionAnswer extends Model
{
    public function testQuestion()
    {
        return $this->hasOne(TestQuestion::class);
    }

    public function testQuestionOptionAnswer()
    {
        return $this->hasOne(TestQuestionOption::class);
    }

    public function testSession()
    {
        return $this->belongsTo(UserTestSession::class);
    }
}
