<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    protected $fillable = [
        'name',
        'duration_minutes',
        'created_by',
        'total_points'
    ];
    //
    public function testQuestions()
    {
        return $this->hasMany(TestQuestion::class);
    }
    
    public function user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
}
