<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestQuestion extends Model
{
    const TYPE_MULTIPLE = 0;
    const TYPE_TRUE_OR_FALSE = 1;

    protected $visible = [
        'question',
        'id',
        'testQuestionOptions',
    ];

    protected $fillable = [
        'question',
        'type',
        'points'
    ];

    public function test()
    {
        return $this->belongsTo(Test::class);
    }

    public function testQuestionOptions()
    {
        return $this->hasMany(TestQuestionOption::class);
    }
}
