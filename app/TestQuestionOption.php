<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestQuestionOption extends Model
{
    protected $visible = [
        'option',
        'id'
    ];

    protected $fillable = [
        'option',
        'correct'
    ];

    public function testQuestion()
    {
        return $this->belongsTo(TestQuestion::class);
    }
}
