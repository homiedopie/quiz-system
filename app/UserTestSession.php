<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTestSession extends Model
{
    //
    public function test()
    {
        return $this->hasOne(Test::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
