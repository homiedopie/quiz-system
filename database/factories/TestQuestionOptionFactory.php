<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\TestQuestionOption;
use Faker\Generator as Faker;

$factory->define(TestQuestionOption::class, function (Faker $faker) {
    return [
        'option' => $faker->text(10),
        'created_at' => today()
    ];
});
