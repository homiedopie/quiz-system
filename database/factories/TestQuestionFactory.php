<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\TestQuestion;
use Faker\Generator as Faker;

$factory->define(TestQuestion::class, function (Faker $faker) {
    return [
        'question' => $faker->text(50),
        'type' => TestQuestion::TYPE_MULTIPLE,
        'points' => 5,
        'created_at' => today()
    ];
});
