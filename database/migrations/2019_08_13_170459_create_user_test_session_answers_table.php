<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTestSessionAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_test_session_answers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_test_session_id');
            $table->unsignedBigInteger('test_id');
            $table->unsignedBigInteger('test_question_id');
            $table->unsignedBigInteger('test_question_option_id');
            $table->timestamp('date_answered');
            $table->boolean('correct');
            $table->integer('points');

            $table->foreign('user_test_session_id')
                ->references('id')->on('user_test_sessions');

            $table->foreign('test_question_option_id')
                ->references('id')->on('test_question_options');

            $table->foreign('test_question_id')
                ->references('id')->on('test_questions');

            // All three fields must be unique so that no 2 answers per question
            // But if you allow multiple answers for each question, remove this
            // $table->unique(['user_test_session_id', 'test_id', 'test_question_id']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_test_session_answers');
    }
}
