<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Test;
use App\TestQuestion;
use App\TestQuestionOption;

class UserTestTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(User::class, 1)->create()->each(function ($user) {
            \Log::info('Creating Tests for User: '. $user->id);
            $user->tests()->saveMany(
                factory(Test::class, 10)
                    ->make([
                        'total_points' => 50
                    ])
            );

            $user->tests->each(function ($test) {
                // Set questions, 20 questions per test
                \Log::info('Creating Test Questions for Test: '. $test->id);
                $test
                    ->testQuestions()
                    ->saveMany(factory(TestQuestion::class, 20)->make());

                $test->testQuestions->each(function ($question) {
                    // Set options
                    \Log::info('Creating Test Question Options for Test Question: '. $question->id);
                    
                    $question
                        ->testQuestionOptions()
                        ->saveMany(
                            // Create 4 options per question
                            factory(TestQuestionOption::class, 4)
                                ->make()
                        );

                    $answer = false;
                    $question->testQuestionOptions->first(function ($option) use (&$answer) {
                        if (!$answer) {
                            \Log::info('Setting answer for Test Question Option: '. $option->id);
                            // set first answer as correct answer
                            $answer = true;
                            $option->correct = true;
                            $option->save();
                        }
                    });
                });
            });
        });
    }
}
