# quiz-system

Laravel Quiz System

1. Setup `.env` file - copy `.env.example` to `.env`
2. Create a new database for the quiz system
3. Fill up the necessary details on `.env`
4. Run `php artisan migrate:install` to create the migration repository
5. Run `php artisan migrate --seed` to run the migration scripts and the seeders
6. Run `php artisan serve` to check locally
7. Import the Postman Collection located at `postman` folder
8. Try checking the structure

9. You can pass a raw json as input with that structure or form data with array being passed